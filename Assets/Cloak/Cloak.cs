﻿using UnityEngine;
using System.Collections;

public class Cloak : MonoBehaviour {
    public  bool isVisible = false;
    public  Material refractionMaterial;
    public  Material dissolveMaterial;
    public  AudioClip cloakEngagedClip;
    public  AudioClip cloakDisengagedClip;
    public  bool enablePlaySound = true;
    public  Texture[] dissolveMaps;
    private float resolveTime = 0;
    private float dissolveTime = 0;
    // Use this for initialization
    void Start() {
        refractionMaterial = GetComponent<Renderer>().materials[1];
        dissolveMaterial = GetComponent<Renderer>().materials[0];
        cloakDisengagedClip = GetComponent<AudioSource>().clip;
        

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * 3);
        if (Input.GetKeyDown("f"))
        {
            int index;
            if (!isVisible)
            {
                cloakEngagedClip = GetComponent<AudioSource>().clip;
                index = Random.Range(0, dissolveMaps.Length);
                GetComponent<Renderer>().material.SetTexture("_SliceGuide", dissolveMaps[index]);
            }
            else
            {
                cloakDisengagedClip = GetComponent<AudioSource>().clip;
                index = Random.Range(0, dissolveMaps.Length);
                GetComponent<Renderer>().material.SetTexture("_SliceGuide", dissolveMaps[index]);
            }
            GetComponent<AudioSource>().Play();
            isVisible = !isVisible;
        }

        turnVisible();
        turnInvisible();
    }
    void turnVisible()
    {
        if (isVisible)
        {
            resolveTime += Time.deltaTime;
            refractionMaterial.SetFloat("_BumpAmt", Mathf.Lerp(0f, 70f, resolveTime - 0.3f));
            dissolveMaterial.SetFloat("_SliceAmount", Mathf.Lerp(0.0f, 1.0f, resolveTime));
        }
        else
        {
            resolveTime = 0;
        }
    }

    void turnInvisible()
    {
        if (!isVisible)
        {
            dissolveTime += Time.deltaTime;
            refractionMaterial.SetFloat("_BumpAmt", Mathf.Lerp(70f, 0f, dissolveTime + 0.3f));
            dissolveMaterial.SetFloat("_SliceAmount", Mathf.Lerp(1.0f, 0.0f, dissolveTime));
        }
        else
        {
            dissolveTime = 0;
        }
    }
}