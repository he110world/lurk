﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeAwayText : MonoBehaviour {

    public Text fadeText;
    float fadeDuration = 4.5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Time.time > fadeDuration)
        {
            Destroy(gameObject);
        }
        Color myColor = fadeText.color;
        float ratio = Time.time / fadeDuration;
        myColor.a = Mathf.Lerp(1, 0, ratio);
        fadeText.color = myColor;
	}
}
