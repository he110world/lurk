﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class CharacterMovement : MonoBehaviour {
    public Animator anim;
    float moveZ;
    float moveX;
    bool isWalking;
    bool isCrouching;
    bool isSprinting;
    bool isHyper;
	// Use this for initialization
	void Start () {
       anim = GetComponent<Animator>();
        isWalking = false;
        isCrouching = false;
        isSprinting = false;
        isHyper = false;
		//GameManager.instance.playerScript.UpdateMovementState(Player.MOVEMENT_STATE.MOVING);
	}
	
	// Update is called once per frame
	void Update () {
       
        Movement();
       // Debug.Log(isCrouching);
        anim.SetBool("isCrouching", isCrouching);
        anim.SetBool("isWalking", isWalking);
        anim.SetBool("isSprinting", isSprinting);
    }

    void Movement()
    {
		/*if (GameManager.instance.playerScript.GetMovementState() == Player.MOVEMENT_STATE.STOPPED)
		{
			moveX = 0;
			moveZ = 0;
			Rigidbody temp = GetComponent<Rigidbody>();
			temp.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
			anim.speed = 0.0f;
		}
		else
		{*/
			Rigidbody temp = GetComponent<Rigidbody>();
			temp.constraints = RigidbodyConstraints.None;
			temp.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
			anim.speed = 1.0f;

			// get movement in x and y
			moveX = Input.GetAxis("Horizontal");
			moveZ = Input.GetAxis("Vertical");
			// set it in the animator to move character
			anim.SetFloat("X", moveX);
			anim.SetFloat("Z", moveZ);
			// check if walking 
			if (Input.GetKey(KeyCode.L))
			{
				isWalking = true;
			}
			else
				isWalking = false;
			// check if Sprinting
			if (Input.GetKey(KeyCode.Alpha1))
				isHyper = true;
			if (Input.GetKey(KeyCode.LeftShift))
			{
				isSprinting = true;

				// hyper mode will cus problems for now u can remove just wanted a small implementation
				// of it for now. It can bee commented out if not needed.
				if (isSprinting && isHyper)
					anim.speed = 1.6f;
				else
					anim.speed = 1f;
			}
			else
				isSprinting = false;

			// check if crouching
			if (Input.GetKeyDown(KeyCode.C))
			{
				if (!isCrouching)
					isCrouching = true;
				else if (isCrouching)
					isCrouching = false;
			}

			// set walking in animator

		//}
	}
}
