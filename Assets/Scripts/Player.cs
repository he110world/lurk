﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class Player : MonoBehaviour
    {
        //bool camOnCharacter;
		MOVEMENT_STATE movementState;
		CAMERA_VIEW cameraView;
        GameObject drone;
        ThirdPersonCharacter mainPlayer;

		public float maxHealth = 100;
		public AudioClip deathNoise;
		private AudioSource audioSource;
		private float health;

		private float timeBeforeRegen = 8.0f;
		private float healthPerSecond = 30.0f;
		private float lastTimeDamaged;

		public GameObject imageOverlayObject;
		private Image bloodOverlay;
		private bool isDeathSoundPlaying = false;

		// Movement state, is the character allowed to move?
		public enum MOVEMENT_STATE
		{
			MOVING, STOPPED
		}

		public enum CAMERA_VIEW
		{
			PLAYER, DRONE
		}

        // Use this for initialization
        void Start()
        {
            drone = GameObject.FindGameObjectWithTag("Drone");
            mainPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<ThirdPersonCharacter>();
			audioSource = GetComponent<AudioSource>();
			movementState = MOVEMENT_STATE.MOVING;
			cameraView = CAMERA_VIEW.PLAYER;
			health = maxHealth;

			bloodOverlay = imageOverlayObject.GetComponent<Image>();
			SetHealth(100);
		}

        // Update is called once per frame
        void Update()
        {
			if (movementState == MOVEMENT_STATE.STOPPED)
			{
		//		mainPlayer.Move(new Vector3(0, 0, 0), false, false); // Stops the character from moving further
			}

			if (IsDamaged() && Time.time - lastTimeDamaged >= timeBeforeRegen)
			{
				SetHealth(health + (healthPerSecond * Time.deltaTime));
			}
		}

		public void UpdateMovementState(MOVEMENT_STATE newState)
		{
			movementState = newState;
		}

		public void SwitchCameraView(CAMERA_VIEW newCamera)
		{ 
			if (newCamera == CAMERA_VIEW.PLAYER)
			{
				UpdateMovementState(MOVEMENT_STATE.MOVING);
				mainPlayer.GetComponent<ThirdPersonUserControl>().enabled = true;
				drone.GetComponent<DroneMovement>().enabled = false;
			}
			else if (newCamera == CAMERA_VIEW.DRONE)
			{
				UpdateMovementState(MOVEMENT_STATE.STOPPED);
				mainPlayer.GetComponent<ThirdPersonUserControl>().enabled = false;
				drone.GetComponent<DroneMovement>().enabled = true;
			}
			cameraView = newCamera;
		}

		public CAMERA_VIEW GetCameraState()
		{
			return cameraView;
		}

		public MOVEMENT_STATE GetMovementState()
		{
			return movementState;
		}

		public bool IsCameraOnCharacter()
		{
			return cameraView == CAMERA_VIEW.PLAYER;
		}

		// Functions related to health
		public bool IsDead()
		{
			return health <= 0;
		}

		public bool TakeDamage(float damage)
		{
			SetHealth(health - damage);
			lastTimeDamaged = Time.time;
			return IsDead();
		}

		public void SetHealth(float newHealth)
		{
			health = newHealth;
			if (health <= 0)
			{
				health = 0;
				UpdateMovementState(MOVEMENT_STATE.STOPPED);

				if (!isDeathSoundPlaying)
				{
					audioSource.PlayOneShot(deathNoise, 1.0f);
					isDeathSoundPlaying = true;
				}
			}
			else
			{
				UpdateMovementState(MOVEMENT_STATE.MOVING);
			}

			if (health >= maxHealth)
			{
				isDeathSoundPlaying = false;
				health = maxHealth;
			}

			//Debug.Log("Health: " + health);
			bloodOverlay.color = new Color(bloodOverlay.color.r, bloodOverlay.color.g, bloodOverlay.color.b, 1 - (health / maxHealth));
		}

		private bool IsDamaged()
		{
			return health < maxHealth;
		}
	}
}
