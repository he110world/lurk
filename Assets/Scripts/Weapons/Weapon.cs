﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

abstract public class Weapon : MonoBehaviour
{
	public AudioClip shotSound;
	public float timeBetweenShots;
	public GameObject wallDamageDecal;
	public int damage;

	protected new AudioSource audio;
	protected float lastTimeShot;

	public void Start()
	{
		lastTimeShot = Time.time - timeBetweenShots;
		audio = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();
	}

	public virtual void Update()
	{
		if (Input.GetMouseButton(0) && Time.time - lastTimeShot >= timeBetweenShots)
		{
			audio.PlayOneShot(shotSound, 1.0f);
			lastTimeShot = Time.time;
			Shoot();
		}
	}

	public abstract void Shoot();
}