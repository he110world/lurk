﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class SilencedPistol : Weapon
{
	public GameObject flares;

	public override void Shoot()
	{
		RaycastHit hit;
		if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
		{
			if (hit.transform.gameObject.tag == "Enemy") // TAGS for raycasting change this later when doing actual damage.
			{
				EnemyAi eAI = hit.transform.gameObject.GetComponent<EnemyAi>();
				eAI.SetHealth(eAI.GetHealth() - damage);
				//Debug.Log(eAI.GetHealth());
			}
			else
			{
				Vector3 direction = hit.point - transform.position;
				direction.Normalize();
				SpawnDecalAndParticles(hit.point - direction * 0.01f, Quaternion.FromToRotation(Vector3.up, hit.normal));
			}
		}
	}

	void SpawnDecalAndParticles(Vector3 spawnPosition, Quaternion angle)
	{
		GameObject wallDecal = (GameObject)Instantiate(wallDamageDecal, spawnPosition, angle);
		//NetworkServer.Spawn(wallDecal);
		Destroy(wallDecal, 20);

		GameObject temp = (GameObject)Instantiate(flares, spawnPosition, Quaternion.identity);
		//NetworkServer.Spawn(temp);
		ParticleSystem ps = temp.GetComponent<ParticleSystem>();
		ps.Play();
	}
}
