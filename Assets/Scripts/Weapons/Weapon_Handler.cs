﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GunHandler : MonoBehaviour
{
	const int NUM_WEAPONS = 2;
	enum WEAPON_TYPE
	{
		SILENCED_PISTOL = 0, RIFLE = 1
	};

	public GameObject[] weapons;
	public AudioClip switch_weapon_sound;

	private WEAPON_TYPE current_weapon = (WEAPON_TYPE)0;
	private GameObject[] current_model;

	private new AudioSource audio;
	//private PhaseGun phaseGunScript;
	//private RocketLauncher rocketLauncherScript;

	// Use this for initialization
	void Start()
	{
		current_model = new GameObject[NUM_WEAPONS];
		//weapon_scripts = new Weapon[NUM_WEAPONS];

		//phaseGunScript = GetComponent<PhaseGun>();
		//rocketLauncherScript = GetComponent<RocketLauncher>();
		audio = GetComponent<AudioSource>();
	}

	void Update()
	{
		int prevWeapon;

		// Switch between weapons.
		if (Input.GetKeyDown(KeyCode.F))
		{
			audio.PlayOneShot(switch_weapon_sound, 1.0f);
			prevWeapon = (int)current_weapon;
			current_weapon++;
			current_weapon = (WEAPON_TYPE)((int)current_weapon % NUM_WEAPONS);
			//CmdSetCurrentWeapon(prevWeapon, (int)current_weapon);
			SetCurrentWeaponScript((int)current_weapon);
		}

		if (Input.GetKeyDown(KeyCode.Alpha1) && current_weapon != (WEAPON_TYPE)0)
		{
			audio.PlayOneShot(switch_weapon_sound, 1.0f);
			prevWeapon = (int)current_weapon;
			current_weapon = (WEAPON_TYPE)0;
			//CmdSetCurrentWeapon(prevWeapon, (int)current_weapon);
			SetCurrentWeaponScript((int)current_weapon);
		}

		if (Input.GetKeyDown(KeyCode.Alpha2) && current_weapon != (WEAPON_TYPE)1)
		{
			audio.PlayOneShot(switch_weapon_sound, 1.0f);
			prevWeapon = (int)current_weapon;
			current_weapon = (WEAPON_TYPE)1;
			SetCurrentWeaponScript((int)current_weapon);
		}
	}

	void SetCurrentWeaponScript(int index)
	{
		if (index == 0)
		{
		//	phaseGunScript.enabled = true;
		//	rocketLauncherScript.enabled = false;
		}
		else
		{
		//	phaseGunScript.enabled = false;
		//	rocketLauncherScript.enabled = true;
		}
	}

	/*[Command]
	void CmdSetCurrentWeapon(int prevModel, int newModel)
	{
		if (current_model[prevModel])
			Destroy(current_model[prevModel]);

		current_model[newModel] = Instantiate(weapons[newModel]);
		current_model[newModel].transform.parent = transform.FindChild("FirstPersonCharacter").transform;
		NetworkServer.SpawnWithClientAuthority(current_model[newModel], connectionToClient);
		RpcSyncObject(gameObject, current_model[newModel], newModel);

		if (newModel == 0)
		{
			current_model[0].transform.localPosition = new Vector3(0.5f, -0.3f, 0.6f);
			current_model[0].transform.localRotation = Quaternion.Euler(0, -90, 10);
		}
		else if (newModel == 1)
		{
			current_model[1].transform.localPosition = new Vector3(0.5f, -0.3f, -0.1f);
			current_model[1].transform.localRotation = Quaternion.Euler(-5, -10, 0);
		}
	}*/
}
