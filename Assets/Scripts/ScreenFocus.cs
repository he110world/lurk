﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class ScreenFocus : MonoBehaviour
{
	[SerializeField] private MouseLook m_MouseLook;

	private Camera m_Camera;
	private GameObject m_ModelToRotate;
	private bool playerFollowCursor;

	// Use this for initialization
	void Start ()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		m_Camera = Camera.main;
		m_ModelToRotate = gameObject;
		m_MouseLook.Init(m_ModelToRotate.transform, m_Camera.transform);
		playerFollowCursor = true;
	}

	private void RotateView()
	{
		if (playerFollowCursor)
			m_MouseLook.LookRotation(m_ModelToRotate.transform, m_Camera.transform);
	}

	public void ChangeModelToRotate(GameObject model)
	{
		m_ModelToRotate = model;
	}

	public void SetCamera(Camera newCamera)
	{
		m_Camera = newCamera;
	}

	// Update is called once per frame
	void Update ()
	{
		RotateView();
		if (Input.GetKeyDown("escape"))
		{
			//playerFollowCursor = false;
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}

		if (Input.GetMouseButtonDown(0))
		{
			playerFollowCursor = true;
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
	}
}
