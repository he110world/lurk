﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class LastPlayerSighting : MonoBehaviour {

    public Vector3 pos = new Vector3(1000, 1000, 1000);
    public Vector3 resetPosition = new Vector3(1000, 1000, 1000);
	public EnemyAi[] enemyAI;
	private Vector3[] initialAIPositions;

    void Start()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		enemyAI = new EnemyAi[enemies.Length];
		initialAIPositions = new Vector3[enemies.Length];
		for (int i = 0; i < enemies.Length; i++)
		{
			enemyAI[i] = enemies[i].GetComponent<EnemyAi>();
			initialAIPositions[i] = enemies[i].transform.position;
		}
	}

	public void ResetAIPositions()
	{
		for (int i = 0; i < enemyAI.Length; i++)
		{
			enemyAI[i].transform.position = initialAIPositions[i];
			enemyAI[i].SetWayPointIndice(0);
		}
	}

	public void AlertOtherAI()
	{
		for (int i = 0; i < enemyAI.Length; i++)
		{
			enemyAI[i].SetState(EnemyAi.AI_STATE.CHASING);
			enemyAI[i].SetNewDestination(pos);
		}
	}

	public void SetAllToPatrol()
	{
		for (int i = 0; i < enemyAI.Length; i++)
		{
			pos = resetPosition;
			enemyAI[i].SetPersonalLastSighting(resetPosition);
			enemyAI[i].SetChaseTimer(0);
			enemyAI[i].SetState(EnemyAi.AI_STATE.PATROLLING);
		}
	}
}
