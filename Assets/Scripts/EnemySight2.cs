﻿using UnityEngine;
using System.Collections;

public class EnemySight2 : MonoBehaviour {

    public float fieldOfViewAngle = 110f;
    public bool playerInSight;
    public Vector3 personalLastSighting;
    private NavMeshAgent nav;
    private SphereCollider col;
	private LastPlayerSighting globalLastPlayerSighting;
    private GameObject player;
    public Vector3 globalPrevSighting;
    bool isCloaked = false;
    public Cloak cloak;
	// Use this for initialization
	void Awake () {
        nav = GetComponent<NavMeshAgent>();
        col = GetComponent<SphereCollider>();
		globalLastPlayerSighting = GameObject.FindGameObjectWithTag("GM").GetComponent<LastPlayerSighting>();
        player = GameObject.FindGameObjectWithTag("Player");
        personalLastSighting = globalLastPlayerSighting.resetPosition;
		globalPrevSighting = globalLastPlayerSighting.resetPosition;
       
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (globalLastPlayerSighting.pos != globalPrevSighting)
		{
			personalLastSighting = globalLastPlayerSighting.pos;
		}

		globalPrevSighting = globalLastPlayerSighting.pos;
        isCloaked = cloak.isVisible;
        Debug.Log(isCloaked);
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject == player)
        {
			playerInSight = false;
            Vector3 direction = other.transform.position - transform.position;
            float angle = Vector3.Angle(direction, transform.forward);
			Debug.DrawRay(transform.position + transform.up, direction, Color.red, 0.01f);

			if (angle < fieldOfViewAngle * 0.5f)
            {
                RaycastHit hit;
				if (Physics.Raycast(transform.position + transform.up, direction.normalized, out hit, col.radius))
                {
					// PLAYER DETECTED
					if (hit.collider.gameObject == player && !isCloaked)
					{
						playerInSight = true;
						globalLastPlayerSighting.pos = player.transform.position;
					}
                }
            }
		}
	}

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
            playerInSight = false;
        //Debug.Log(playerInSight);
    }
}
