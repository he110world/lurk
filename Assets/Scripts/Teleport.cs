﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour
{
	public KeyCode scanKey;

	// PLAYING is when the player is any time the player is not doing the teleport ability.
	private enum STATE
	{
		PLAYING, SCANNING, TELEPORTING
	};

	private STATE currentState;

	// Use this for initialization
	void Start ()
	{
		currentState = STATE.PLAYING;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (Input.GetKey(scanKey))
		{
			currentState = STATE.SCANNING;
		}
	}
}
