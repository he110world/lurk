﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class DroneMovement : MonoBehaviour
    {
        //Variables
        public float speed = 6.0F;
        ThirdPersonCharacter mainPlayer;
		Player playerControl;
        GameObject Drone;

        void Awake()
        {
            mainPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<ThirdPersonCharacter>();
			playerControl = mainPlayer.GetComponent<Player>();
            Drone = GetComponent<GameObject>();
        }
        void Update()
        {
            Control();
        }

        void Control()
        {
            if (!playerControl.IsCameraOnCharacter())
            {
                GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                if (Input.GetKey(KeyCode.W))
                {
                    transform.position += new Vector3(0.0f, 0.0f, speed * Time.deltaTime);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    transform.position -= new Vector3(0.0f, 0.0f, speed * Time.deltaTime);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    transform.position -= new Vector3(speed * Time.deltaTime, 0.0f, 0.0f);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    transform.position += new Vector3(speed * Time.deltaTime, 0.0f, 0.0f);
                }
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    transform.position += new Vector3(0.0f, speed * Time.deltaTime, 0.0f);
                }
                if (Input.GetKey(KeyCode.Space))
                {
                    transform.position -= new Vector3(0.0f, speed * Time.deltaTime, 0.0f);
                }
                Vector3 pos = transform.position;
            }
        }
    }
}