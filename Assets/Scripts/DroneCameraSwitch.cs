﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class DroneCameraSwitch : MonoBehaviour
    {
		GameObject player;
		GameObject drone;
        Camera cam;
		Camera DroneCAM;

		ThirdPersonCharacter mainCharacter;
		Player playerControl;
		ScreenFocus screenFocus;

        // Use this for initialization
        void Start()
        {
			player = gameObject;
			drone = GameObject.FindGameObjectWithTag("Drone");

			cam = GetComponentInChildren<Camera>();
			DroneCAM = drone.GetComponentInChildren<Camera>();
            mainCharacter = GetComponent<ThirdPersonCharacter>();
			playerControl = mainCharacter.GetComponent<Player>();
			screenFocus = mainCharacter.GetComponent<ScreenFocus>();

			DroneCAM.enabled = false;
		}

        // Update is called once per frame
        void Update()
        {
            SwitchView();
        }

        void SwitchView()
        {
            if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                if (playerControl.IsCameraOnCharacter())
                {
					playerControl.SwitchCameraView(Player.CAMERA_VIEW.DRONE);
                    cam.enabled = false;
                    DroneCAM.enabled = true;
					screenFocus.SetCamera(DroneCAM);
					screenFocus.ChangeModelToRotate(drone);
				}
                else
                {
					playerControl.SwitchCameraView(Player.CAMERA_VIEW.PLAYER);
                    cam.enabled = true;
                    DroneCAM.enabled = false;
					screenFocus.SetCamera(Camera.main);
					screenFocus.ChangeModelToRotate(player);
				}
            }
        }
    }
}