﻿using UnityEngine;
using System.Collections;

public class DynamicCrosshair : MonoBehaviour {
	private Transform playerTransform;
	private float playerSpeed;
	private float nextCaptureTime;
	private float captureInterval = 0.5f;
	private Vector3 lastPosition;
	public Animator crosshairAnimator;

	// Use this for initialization
	void Start ()
	{
		playerTransform = transform.parent.transform;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (Time.time > nextCaptureTime)
		{
			nextCaptureTime = Time.time + captureInterval;
			playerSpeed = (playerTransform.position - lastPosition).magnitude / captureInterval;
			lastPosition = playerTransform.position;
		}

		if (crosshairAnimator != null)
		{
			crosshairAnimator.SetFloat("Speed", playerSpeed);
		}
	}
}
