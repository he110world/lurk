﻿using UnityEngine;
using System.Collections;

//TO DO: MAKE SO AI ROTATES TOWARDS PLAYER SMOOTHLY
//	     WHEN CHASING, MAKE SURE AI DOESNT GET ALL UP IN YOUR FACE
//	     SHOOTING, MAKE SURE AI DOESNT MOVE WHILE SHOOTING

namespace UnityStandardAssets.Characters.ThirdPerson
{
	public class EnemyAi : MonoBehaviour
	{
		public enum AI_STATE
		{
			SHOOTING, CHASING, PATROLLING, SEARCHING, DEAD
		}

		public float patrolSpeed = 0.5f;
		public float chaseSpeed = 1f;
		public float ChaseWaitTime = 5f;
		public float patrolWaitTime = 1f;
		public Transform[] patrolWayPoints;
		public ThirdPersonCharacter character;

		private float shootingDistance = 10.0f;
		private float timeBetweenShots = 1.7f;
		private float shootingTimeVariance = 0.5f; // Some randomness, so AI shoots +/- of the timeBetweenShots + variance.
		public float shootingHitChance = 0.8f; // Value from 0 - 1, how likely the AI will shoot and hit the target.
		private float damageDone = 30.0f;
		private float shootingTimer = 0.0f;
		private float nextShotDelay;
		private bool firstShotFired = false;

		private EnemySight2 enemySight;
		private NavMeshAgent nav;
		private Transform player;
		private Player playerScript;
		private LastPlayerSighting globalLastPlayerSighting;
		private float chaseTimer;
		private float patrolTimer;
		private int wayPointInd;
		private AI_STATE currentState;
		private Animator animator;

		// Related to shooting.
		private AudioSource audioSource;
		private float health = 100;
		public AudioClip shootingSound;
		public AudioClip deathSound;
		private bool isDeathSoundPlaying = false;

		// Use this for initialization
		void Awake()
		{
			enemySight = GetComponent<EnemySight2>();
			nav = GetComponent<NavMeshAgent>();
			player = GameObject.FindGameObjectWithTag("Player").transform;
			audioSource = GetComponent<AudioSource>();
			globalLastPlayerSighting = GameObject.FindGameObjectWithTag("GM").GetComponent<LastPlayerSighting>();
			character = GetComponent<ThirdPersonCharacter>();
			playerScript = player.GetComponent<Player>();

			nav.updatePosition = true;
			nav.updateRotation = false;

			currentState = AI_STATE.PATROLLING;
			animator = GetComponent<Animator>();

			nextShotDelay = 0.0f;
		}

		public void SetState(AI_STATE newState)
		{
			currentState = newState;
		}
	
		public void SetNewDestination(Vector3 destination)
		{
			if (nav)
				nav.SetDestination(destination);
		}

		// Update is called once per frame
		void FixedUpdate()
		{
			if (currentState == AI_STATE.PATROLLING)
			{
			//	Debug.Log(transform.name + " patrolling");
				Patrolling();
			}
			else if (currentState == AI_STATE.SHOOTING)
			{
	//			Debug.Log(transform.name + " shooting");
				Shooting();
			}
			else if (currentState == AI_STATE.SEARCHING)
			{
			//	Debug.Log(transform.name + " searching");
				Searching();
			}
			else if (currentState == AI_STATE.CHASING)
			{
				//Debug.Log(transform.name + " chasing");
				Chasing();
			}
			else if (currentState == AI_STATE.DEAD)
			{
				nav.Stop();
				character.Move(Vector3.zero, false, false);
				Destroy(transform.gameObject, 1.0f);
			}
		}

		void Patrolling()
		{
			// if (playerInSight)
			// currentState = AI_STATE.CHASING;
			// if (sound made)
			// currentState = AI_STATE.SEARCHING;
			nav.Resume();
			nav.speed = patrolSpeed;
			if (nav.destination == globalLastPlayerSighting.resetPosition || nav.remainingDistance < nav.stoppingDistance)
			{
				patrolTimer += Time.deltaTime;
				if (patrolTimer >= patrolWaitTime)
				{
					if (wayPointInd == patrolWayPoints.Length - 1)
					{
						wayPointInd = 0;
					}
					else
					{
						wayPointInd++;
					}

					patrolTimer = 0f;
				}
			}
			else
			{
				patrolTimer = 0f;
			}

			nav.SetDestination(patrolWayPoints[wayPointInd].position);
			character.Move(nav.desiredVelocity, false, false);

			if (enemySight.playerInSight && !playerScript.IsDead())
			{
				currentState = AI_STATE.CHASING;
				globalLastPlayerSighting.AlertOtherAI();
			}
		}

		void Shooting()
		{
			nav.Stop();
			character.Move(Vector3.zero, false, false);
			transform.rotation = Quaternion.Slerp(transform.rotation,
				Quaternion.LookRotation(enemySight.personalLastSighting - transform.position),
				nav.angularSpeed * Time.deltaTime);

			// If the player is not in shooting range, and also
			// the AI is not in the middle of firing a shot.
			if (!IsPlayerInShootingRange() && shootingTimer == 0)
			{
				currentState = AI_STATE.CHASING;
			}
			else
			{
				shootingTimer += Time.deltaTime;
				if (shootingTimer >= nextShotDelay)
				{
					shootingTimer = 0.0f;
					audioSource.PlayOneShot(shootingSound, 1.0f);

					if (!firstShotFired)
					{
						firstShotFired = true;
						nextShotDelay = timeBetweenShots + Random.Range(-shootingTimeVariance, shootingTimeVariance);
					}

					if (Random.Range(0.0f, 1.0f) <= shootingHitChance // If we are within the random shot chance,
																      // the player will be hit.
						&& IsPlayerInShootingRange()) // This is to make sure that the player is not behind
													  // a wall when the bullet is actually shot.
					{
						nextShotDelay = timeBetweenShots + Random.Range(-shootingTimeVariance, shootingTimeVariance);
						playerScript.TakeDamage(damageDone);
					}
				}	
			}
		}

		void Searching()
		{
			// if (Area searched)
			// currentState = AI_STATE.PATROL;
		}

		void Chasing()
		{
			//if (!enemySight.playerInSight)
			// if (all AI Lost sight of player)
			// currentState = AI_STATE.SEARCHING;
			// if (in shooting range and in line of sight)
			// currentState = AI_STATE.SHOOTING
			// else
			// currentState = AI_STATE.CHASING
			Vector3 sightingDeltaPos = enemySight.personalLastSighting - transform.position;
			if (sightingDeltaPos.sqrMagnitude >= 4f)
			{
				nav.SetDestination(enemySight.personalLastSighting);
			}

			nav.Resume();
			nav.speed = chaseSpeed;
			character.Move(nav.desiredVelocity, false, false);

			if (nav.remainingDistance <= nav.stoppingDistance)
			{
				chaseTimer += Time.deltaTime;
				if (chaseTimer >= ChaseWaitTime)
				{
					globalLastPlayerSighting.pos = globalLastPlayerSighting.resetPosition;
					enemySight.personalLastSighting = globalLastPlayerSighting.resetPosition;
					chaseTimer = 0f;
					currentState = AI_STATE.PATROLLING;
				}
			}
			else
				chaseTimer = 0f;

			if (IsPlayerInShootingRange())
				currentState = AI_STATE.SHOOTING;
		}

		bool IsPlayerInShootingRange()
		{
			// If in shooting range.
			// If player is in line of sight
			// and the raycast hits the player.

			RaycastHit[] hits;
			bool isPlayerHitwithRaycast = false;
			bool isAIHitWithRaycast = false;

			// Transform.up so raycast occurs around chest height.
			float chestHeight = 1.0f;
			Vector3 startRaycastPos = transform.position + transform.up * chestHeight;
			Vector3 endRaycastPos = enemySight.personalLastSighting + transform.up * chestHeight;
			Vector3 direction = endRaycastPos - startRaycastPos;

			hits = Physics.RaycastAll(startRaycastPos, direction, shootingDistance);
			Debug.DrawRay(startRaycastPos, direction, Color.red, 0.01f);
			//Debug.Log(Vector3.Distance(nav.destination, transform.position) + " " + shootingDistance);
			if (Vector3.Distance(nav.destination, transform.position) <= shootingDistance)
			{
				for (int i = 0; i < hits.Length; i++)
				{
					if (hits[i].transform.tag == "Player")
						isPlayerHitwithRaycast = true;
					else if (hits[i].transform.tag == "Enemy")
						isAIHitWithRaycast = true;
					else
						return false; // Object is in the way, can't shoot from current position.
				}
			}
			return (isPlayerHitwithRaycast && hits.Length == 1) // Only player is hit with raycast
				|| (isPlayerHitwithRaycast && isAIHitWithRaycast && hits.Length >= 2); // Player hiding behind enemy.
		}

		public void SetChaseTimer(float time)
		{
			chaseTimer = time;
		}

		public void SetPersonalLastSighting(Vector3 vec)
		{
			enemySight.personalLastSighting = vec;
		}

		public void SetWayPointIndice(int index)
		{
			wayPointInd = index;
		}

		public void SetHealth(float newHealth)
		{
			health = newHealth;
			// CHANGE THIS LATER
			currentState = AI_STATE.CHASING;
			enemySight.personalLastSighting = player.transform.position;
			globalLastPlayerSighting.AlertOtherAI();

			if (health <= 0)
			{
				currentState = AI_STATE.DEAD;

				if (!isDeathSoundPlaying)
				{
					audioSource.PlayOneShot(deathSound, 1.0f);
					isDeathSoundPlaying = true;
				}
			}
		}

		public float GetHealth()
		{
			return health;
		}
	}
}
