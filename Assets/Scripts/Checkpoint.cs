﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {
	void OnTriggerEnter(Collider other)
	{
		if (other.transform.tag == "Player")
		{
			Debug.Log("Checkpoint saved");
			GameManager.instance.lastCheckpoint = transform.position;
		}
	}
}
