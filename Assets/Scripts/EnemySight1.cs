﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class EnemySight1 : MonoBehaviour
    {
        public NavMeshAgent agent;
        public ThirdPersonCharacter character;
        public enum State {
            PATROL,
            CHASE,
            INVESTIGATE
        }
        public State state;
        private bool alive;
        // Variables for Patroling
        public GameObject[] waypoints;
        //for set waypoints
        //private int wayPointInd = 0;

        //for random waypoints
        private int wayPointInd;
        public float patrolSpeed = 0.5f;

        // Variables for chasing 
        public float chaseSpeed = 1f;
        public GameObject target;

        // Variables for INvestigating 
        private Vector3 investigateSpot;
        private float timer = 0;
        public float investigateWait = 10;

        // variables for Sight
        public float heightMultiplier;
        public float sightDist = 10;
        // Use this for initialization
        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();
            // randomizing the points
            waypoints = GameObject.FindGameObjectsWithTag("Waypoints");
            wayPointInd = Random.Range(0,waypoints.Length);


            agent.updatePosition = true;
            agent.updateRotation = false;
            state = EnemySight1.State.PATROL;
            alive = true;
            //Start StateMachine
            StartCoroutine("StateMachine");

            heightMultiplier = 1.36f;
        }
        IEnumerator StateMachine()
        {while (alive)
            {
                switch (state)
                {
                    case State.PATROL:
                        Patrol();
                        break;
                    case State.CHASE:
                        Chase();
                        break;
                    case State.INVESTIGATE:
                        Investigate();
                        break;
                }
                yield return null;
            }
        }
        void Patrol()
        {
            agent.speed = patrolSpeed;
            if (Vector3.Distance(this.transform.position, waypoints[wayPointInd].transform.position) >= 2)
            {
                agent.SetDestination(waypoints[wayPointInd].transform.position);
                character.Move(agent.desiredVelocity, false, false);
            }
            else if (Vector3.Distance(this.transform.position, waypoints[wayPointInd].transform.position) <= 2)
            {
                // for selecting waypoints 
                //wayPointInd += 1;
                //if (wayPointInd > waypoints.Length)
                //{
                //    wayPointInd = 0;
                //}

                // for randomizing point after reaching a point
                wayPointInd = Random.Range(0, waypoints.Length);

            }
            else
            {
                character.Move(Vector3.zero, false, false);
            }
        }
        void Chase()
        {
            agent.speed = chaseSpeed;
            agent.SetDestination(target.transform.position);
            character.Move(agent.desiredVelocity, false, false);
        }
        void Investigate()
        {
            timer += Time.deltaTime;
            agent.SetDestination(this.transform.position);
            character.Move(Vector3.zero, false, false);
            if (timer >= investigateWait)
            {
                state = EnemySight1.State.PATROL;
                timer = 0;
            }
        }
        
        void OnTriggerEnter(Collider coll)
        {
            if (coll.tag == "Player")
            {
                // for automatic chase and not investigate 
                //state = EnemySight1.State.CHASE;
                //target = coll.gameObject;

                // to investigate 
                state = EnemySight1.State.INVESTIGATE;
                investigateSpot = coll.gameObject.transform.position;
            }

        }
        void FixedUpdate()
        {
            RaycastHit hit;
            Debug.DrawRay(transform.position + Vector3.up * heightMultiplier, transform.forward * sightDist, Color.yellow);
            Debug.DrawRay(transform.position + Vector3.up * heightMultiplier, (transform.forward + transform.right).normalized * sightDist, Color.yellow);
            Debug.DrawRay(transform.position + Vector3.up * heightMultiplier, (transform.forward - transform.right).normalized * sightDist, Color.yellow);
            if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, transform.forward, out hit, sightDist))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    state = EnemySight1.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }
            if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, (transform.forward + transform.right).normalized, out hit, sightDist))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    state = EnemySight1.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }
            if (Physics.Raycast(transform.position + Vector3.up * heightMultiplier, (transform.forward - transform.right).normalized, out hit, sightDist))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    state = EnemySight1.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }
        }
    }
}