﻿using UnityEngine;
using System.Collections;

public class CameraSwitch : MonoBehaviour {

    public Camera camera1;
    public Camera camera2;
    float timer = 0.0f;
	GameObject player;
    // Use this for initialization
    void Start () {
        camera1.enabled = false;
        camera2.enabled = true;
		player = GameObject.FindGameObjectWithTag("Player");
	}

    void mainCameraSwitch()
    {
        camera1.enabled = true;
        camera2.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
		if (timer <= 5)
		{
			player.GetComponent<CharacterMovement>().enabled = false;
			player.GetComponent<ScreenFocus>().enabled = false;
			player.GetComponent<SilencedPistol>().enabled = false;
		}
        Debug.Log(timer);
        if (timer >= 5)
        {
			player.GetComponent<CharacterMovement>().enabled = true;
			player.GetComponent<ScreenFocus>().enabled = true;
			player.GetComponent<SilencedPistol>().enabled = true;
			mainCameraSwitch();
            camera2.depth = -3;
            this.enabled = false; 
			  
        }
	}
}
