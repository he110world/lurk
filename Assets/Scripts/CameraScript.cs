﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    public class CameraScript : MonoBehaviour
    {
        public NavMeshAgent agent;
        public ThirdPersonCharacter character;
        public enum State
        {
            PATROL,
            CHASE
            
        }
        private LastPlayerSighting lastPlayerSighting;
        public Vector3 prevPlayerSighting;
        private Vector3 personalLastSighting;
        public State state;
        private bool alive;
        // Variables for Patroling
        public GameObject[] waypoints;
        //for set waypoints
        private int wayPointInd = 0;

        //for random waypoints
        //private int wayPointInd;

        public float patrolSpeed = 0.5f;

        // Variables for chasing 
        public float chaseSpeed = 1f;
        public GameObject target;

        //CameraSight Variables
        public GameObject player;
        public Collider PlayerColl;
        public Camera myCam;
        private Plane[] planes;
        //private EnemyAlertSys EAS;
        bool Detected;

        // Use this for initialization
        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();
            // randomizing the points
            //waypoints = GameObject.FindGameObjectsWithTag("Waypoints");
            //wayPointInd = Random.Range(0, waypoints.Length);

            PlayerColl = player.GetComponent<Collider>();
            lastPlayerSighting = GameObject.FindGameObjectWithTag("GM").GetComponent<LastPlayerSighting>();
            //EAS = GameObject.FindGameObjectWithTag("GM").GetComponent<EnemyAlertSys>();
            personalLastSighting = lastPlayerSighting.resetPosition;
            agent.updatePosition = true;
            agent.updateRotation = false;
            state = CameraScript.State.PATROL;
            alive = true;
            Detected = false;
            //Start StateMachine
            StartCoroutine("StateMachine");
        }
        IEnumerator StateMachine()
        {
            while (alive)
            {
                switch (state)
                {
                    case State.PATROL:
                        Patrol();
                        break;
                    case State.CHASE:
                        Chase();
                        break;
                }
                yield return null;
            }
        }
        void Patrol()
        {
            agent.speed = patrolSpeed;
            if (Vector3.Distance(this.transform.position, waypoints[wayPointInd].transform.position) >= 2)
            {
                agent.SetDestination(waypoints[wayPointInd].transform.position);
                character.Move(agent.desiredVelocity, false, false);
            }
            else if (Vector3.Distance(this.transform.position, waypoints[wayPointInd].transform.position) <= 2)
            {
                // for selecting waypoints 
                wayPointInd += 1;
                if (wayPointInd >= waypoints.Length)
                {
                    wayPointInd = 0;
                }

                // for randomizing point after reaching a point
                //wayPointInd = Random.Range(0, waypoints.Length);

            }
            else
            {
                character.Move(Vector3.zero, false, false);
            }
        }
        void Chase()
        {
            agent.speed = chaseSpeed;
            agent.SetDestination(target.transform.position);
            character.Move(agent.desiredVelocity, false, false);
        }

        void Update()
        {
            Alerted();   
            planes = GeometryUtility.CalculateFrustumPlanes(myCam);
            if (GeometryUtility.TestPlanesAABB(planes, PlayerColl.bounds))
            {
                Debug.Log("Player Sighted");
                CheckForPlayer();
            }
            else
            {
                state = CameraScript.State.PATROL;
            }


        }
     public void CheckForPlayer()
        { 
            RaycastHit hit;
            Debug.DrawRay(myCam.transform.position, transform.forward * 30, Color.green);
            Debug.DrawRay(myCam.transform.position, (transform.forward + transform.right).normalized * 30, Color.green);
            Debug.DrawRay(myCam.transform.position, (transform.forward - transform.right).normalized * 30, Color.green);


            if (Physics.Raycast(myCam.transform.position, transform.forward, out hit, 30))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    Detected = true;
                    lastPlayerSighting.pos = hit.collider.gameObject.transform.position;
                    state = CameraScript.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }
            else if (Physics.Raycast(myCam.transform.position, (transform.forward + transform.right).normalized, out hit, 30))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    Detected = true;
                    lastPlayerSighting.pos = hit.collider.gameObject.transform.position;
                    state = CameraScript.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }
            else if (Physics.Raycast(myCam.transform.position, (transform.forward - transform.right).normalized, out hit, 30))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    Detected = true;
                    lastPlayerSighting.pos = hit.collider.gameObject.transform.position;
                    state = CameraScript.State.CHASE;
                    target = hit.collider.gameObject;
                }
            }
            else
               Detected = false;
        }
        void Alerted()
        {
                if (Detected)
                {
                Debug.Log("I am here!!");
                state = CameraScript.State.CHASE;
                target = player;
                if (transform.position == prevPlayerSighting)
                    {
                        CheckForPlayer();
                        if (!Detected)
                            GetComponent<CameraScript>().state = CameraScript.State.PATROL;
                    }

                }
            }
        }

    }
