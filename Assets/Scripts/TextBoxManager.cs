﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextBoxManager : MonoBehaviour {

    public GameObject textBox;
    public Text theText;
    public TextAsset textFile;
    public string[] textLines;
    public int currentLine;
    public int endAtLine;
    public bool isActive;
    private bool isTyping = false;
    private bool cancelTyping = false;
    public float textSpeed;
	GameObject player;
	// Use this for initialization
	void Start()
    {
		player = GameObject.FindGameObjectWithTag("Player");
		if (textFile != null)
        {
            textLines = (textFile.text.Split('\n'));
        }
        if (endAtLine == 0)
        {
            endAtLine = textLines.Length - 1;
        }
        if (isActive)
        {
            EnableTextBox();
        }
        else
        {
            DisableTextBox();
        }
    }

    void Update()
    {
        if (!isActive)
        {
            return;
        }
       // theText.text = textLines[currentLine];
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (!isTyping)
            {
                currentLine += 1;
                if (currentLine > endAtLine)
                {
                    DisableTextBox();
                }
                else
                {
                    StartCoroutine(TextScroll(textLines[currentLine]));
                }
            }
            else if (isTyping && !cancelTyping)
            {
                cancelTyping = true;
            }
        }
    }

    public IEnumerator TextScroll (string lineOfText)
    {
        int letter = 0;
        theText.text = "";
        isTyping = true;
        cancelTyping = false;
        while (isTyping && !cancelTyping && (letter < lineOfText.Length - 1))
        {
            theText.text += lineOfText[letter];
            letter += 1;
            yield return new WaitForSeconds(textSpeed);
        }
        theText.text = lineOfText;
        isTyping = false;
        cancelTyping = false;
    }

    public void EnableTextBox()
    {
		player.GetComponent<CharacterMovement>().enabled = false;
		player.GetComponent<ScreenFocus>().enabled = false;
		player.GetComponent<SilencedPistol>().enabled = false;
		player.GetComponent<Rigidbody>().freezeRotation = true;
		textBox.SetActive(true);
        isActive = true;
        StartCoroutine(TextScroll(textLines[currentLine]));     
    }

    public void DisableTextBox()
    {
		player.GetComponent<CharacterMovement>().enabled = true;
		player.GetComponent<ScreenFocus>().enabled = true;
		player.GetComponent<SilencedPistol>().enabled = true;
		player.GetComponent<Rigidbody>().freezeRotation = false;
		textBox.SetActive(false);
        isActive = false;
    }

    //use different texts in our game
    public void ReloadScript(TextAsset theText)
    {
        if (theText != null)
        {
            textLines = new string[1];
            textLines = (theText.text.Split('\n'));
        }
    }
}
