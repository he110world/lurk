﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class GameManager : MonoBehaviour
{
	public enum FADE_STATE
	{
		FADE_IN, FADE_OUT, NONE
	}

	public static GameManager instance = null;
	public Vector3 lastCheckpoint;

	public GameObject player;
	public Player playerScript;

	public GameObject fadeGameObject;
	private Image fadeScreen;
	private float fadeInTime = 3.0f;
	private float fadeOutTime;
	private FADE_STATE fadeState;

	private float waitTimeAfterDeath = 5.0f;
	private float timeOfDeath;

	private LastPlayerSighting aiManager;

	// Singleton
	void Awake()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);
	}

	void Start()
	{
		lastCheckpoint = new Vector3(0, 0, 0);
		player = GameObject.FindGameObjectWithTag("Player");
		playerScript = player.GetComponent<Player>();
		aiManager = GetComponent<LastPlayerSighting>();
		timeOfDeath = 0;
		fadeState = FADE_STATE.FADE_IN;
		fadeScreen = fadeGameObject.GetComponent<Image>();
		fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b, 1.0f);
		fadeOutTime = waitTimeAfterDeath;
	}

	void FixedUpdate()
	{
		if (playerScript.IsDead())
		{
			if (timeOfDeath == 0)
			{
				timeOfDeath = Time.time;
				aiManager.SetAllToPatrol();
				fadeState = FADE_STATE.FADE_OUT;
			}

			if (Time.time - timeOfDeath >= waitTimeAfterDeath)
			{
				fadeState = FADE_STATE.FADE_IN;
				timeOfDeath = 0;
				playerScript.SetHealth(playerScript.maxHealth);
				player.transform.position = lastCheckpoint;
				aiManager.ResetAIPositions();
			}
		}

		if (fadeState == FADE_STATE.FADE_IN)
		{
			fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b, fadeScreen.color.a - (1.0f / fadeInTime) * Time.fixedDeltaTime);
			if (fadeScreen.color.a <= 0)
				fadeState = FADE_STATE.NONE;
		}
		else if (fadeState == FADE_STATE.FADE_OUT)
		{
			fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b, fadeScreen.color.a + (1.0f / fadeOutTime) * Time.fixedDeltaTime);
			if (fadeScreen.color.a >= 1)
				fadeState = FADE_STATE.NONE;
		}
	}

	public void SetFadeState(FADE_STATE newState)
	{
		fadeState = newState;
	}
}