﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {
    public Camera[] Cameras;
	// Use this for initialization
	void Start () {
        for (int i = 0; i < Cameras.Length; i++)
        {
            if (Cameras[i].GetComponent<Camera>().tag == "MainCamera")
            {
                Cameras[i].GetComponent<Camera>().enabled = true;
            }
            else
                Cameras[i].GetComponent<Camera>().enabled = false;

        }
        
	}
	
	// Update is called once per frame
	void Update () {
    
    }
}

